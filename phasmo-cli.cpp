#include "es3crypt.hpp"

#include <iostream>
#include <fstream>
#include <string>



bool isEncrypted(const char *path) {
    // Open file
    std::fstream f(path, std::ios::openmode::_S_in | std::ios::openmode::_S_bin);
    // Read first byte
    char firstByte;
    f.read(&firstByte, 1);
    // Determine
    return firstByte != '{';
}

int main(int argc, char **argv) {
    // Static stuff
    const std::string decrypted_file_suffix = ".json";
    // Check args
    if (argc < 2) {
        std::cout << "Usage: " << argv[0] << " <file>" << std::endl;
        return 1;
    }
    const std::string password = "t36gref9u84y7f43g";
    // Get args
    auto file_path = argv[1];
    // Encrypt or decrypt
    if (isEncrypted(file_path)) {
        // Open input and output file
        std::fstream in_file(file_path, std::ios::openmode::_S_in | std::ios::openmode::_S_bin);
        std::fstream out_file(std::string(file_path)+decrypted_file_suffix, std::ios::openmode::_S_out | std::ios::openmode::_S_bin);
        // Decrypt
        ES3Crypt::decrypt(in_file, out_file, password, 2048);
    } else {
        // Open input and output file
        std::string out_file_path(file_path);
        out_file_path.erase(out_file_path.size()-decrypted_file_suffix.size(), decrypted_file_suffix.size());
        std::fstream in_file(file_path, std::ios::openmode::_S_in | std::ios::openmode::_S_bin);
        std::fstream out_file(out_file_path, std::ios::openmode::_S_out | std::ios::openmode::_S_bin);
        // Encrypt
        ES3Crypt::encrypt(in_file, out_file, password, 2048);
    }
}
